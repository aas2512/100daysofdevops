
# LINUX(RHCSA II) / TERRAFORM

## DevOps Learning

1. Linux: Managing Logical Volumes
2. Terraform: Create Instances and get dynamic data from AWS(AMI Images)


### 1. LINUX
#### Managing Logical Volumes

 ***Creating Logical Volumes***

Physical devices<br>
Physical devices are the storage devices used to save data stored in a logical volume. 

Physical volumes (PVs)<br> 
Physical volumes are the underlying "physical" storage used with LVM. You must initialize a device as a physical volume before using it in an LVM system. 

Volume groups (VGs)<br> 
Volume groups are storage pools made up of one or more physical volumes. This is the functional equivalent of a whole disk in basic storage. 

Logical volumes (LVs)<br>
Logical volumes are created from free physical extents in a volume group and provide the "storage" device used by applications, users, and the operating system. LVs are a collection of logical extents (LEs), which map to physical extents, the smallest storage chunk of a PV.

<p align="center">
  <img src="lvm-build.svg">
</p>

Prepare the physical device.<br>
Use parted, gdisk, or fdisk to create a new partition for use with LVM. Physical device only needs to be prepared if there are none prepare.

```
[root@host ~]# parted -s /dev/vdb mkpart primary 1MiB 769MiB
[root@host ~]# parted -s /dev/vdb mkpart primary 770MiB 1026MiB
[root@host ~]# parted -s /dev/vdb set 1 lvm on
[root@host ~]# parted -s /dev/vdb set 2 lvm on
```

Create a physical volume.<br>
`[root@host ~]# pvcreate /dev/vdb2 /dev/vdb1`<br>
This labels the devices /dev/vdb2 and /dev/vdb1 as PVs, ready for allocation into a volume group.

Create a volume group.<br>
`[root@host ~]# vgcreate vg01 /dev/vdb2 /dev/vdb1`<br>
This creates a VG called vg01 that is the combined size, in PE units, of the two PVs /dev/vdb2 and /dev/vdb1.

Create a logical volume.<br>
`[root@host ~]# lvcreate -n lv01 -L 700M vg01`<br>
This creates an LV called lv01, 700 MiB in size, in the VG vg01. 

Add the file system.<br>
`[root@host ~]# mkfs -t xfs /dev/vg01/lv01`

Use mkdir to create a mount point.<br>
`[root@host ~]# mkdir /mnt/data`

Add an entry to the /etc/fstab file:<br>
`/dev/vg01/lv01  /mnt/data xfs  defaults 1 2`

Run mount /mnt/data to mount the file system that you just added in /etc/fstab.<br>
`[root@host ~]# mount /mnt/data`

***Remove the logical volume***

Use lvremove DEVICE_NAME to remove a logical volume that is no longer needed.
`[root@host ~]# lvremove /dev/vg01/lv01`<br>
Unmount the LV file system before running this command. The command prompts for confirmation before removing the LV.

Use vgremove VG_NAME to remove a volume group that is no longer needed.<br>
`[root@host ~]# vgremove vg01`<br>
The VG's physical volumes are freed and made available for assignment to existing or new VGs on the system.

Use pvremove to remove physical volumes that are no longer needed.<br>
`[root@host ~]# pvremove /dev/vdb2 /dev/vdb1`


----

### 2. TERRAFORM

You can pass the AMI ID directly to resouce on Terraform, but this is not best practive, because the ID change between regions on AWS. The right way is get data dynamicaly using "data" component on Terraform.

Ex:

```
data "aws_ami" "devopslearn_latest_ami_linux_image" {
  #get the image most recent
  most_recent = true

  #pass to filter to get images from this owner
  owners = [
    "amazon"
  ]

  #fetch the data from AWS with the parameters below
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
```

Assign the public key to access ssh:

```
resource "aws_key_pair" "devopslearn_ssh_key" {
  key_name   = "server-key"
  public_key = var.ssh_public_key
}
```

Create a instance resource:

```
resource "aws_instance" "devopslearn-server" {

  //REQUIRED ARGUMENTS
  ami           = data.aws_ami.devopslearn_latest_ami_linux_image.id
  instance_type = var.instance_type

  //OPTIONAL ARGUMENTS
  subnet_id = aws_subnet.devopslearn-subnet-1.id
  vpc_security_group_ids = [
    aws_default_security_group.devopslearn-dmz-sg.id
  ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true

  key_name = aws_key_pair.devopslearn_ssh_key.key_name

  user_data = file("entry-script.sh")

  tags = {
    Name : "${var.env_prefix}-ec2-instance"
  }
}

```
<p align="center">
  <img src="result_terraform_apply.png">
</p>

<p align="center">
  <img src="result_terraform_destroy_terraform_cloud.png">
</p>

### Terminal ZSH Personalization
##### Tilix + ZSH + Oh My Zsh + Powerline + PowerLevel9K + Theme + Some Plugins

<p align="center">
  <img src="zsh-terminal.png">
</p>

1. (Req). Install and set the Zsh shell (http://www.zsh.org/​)
2. (Req). Install Oh-My-Zsh (https://github.com/robbyrussell/oh-my...​)
3. (Opt). Install Powerline and set the patched font (https://github.com/powerline/powerline​)
4. (Opt). Install PowerLevel9K and set it as the theme (https://github.com/bhilburn/powerlevel9k​)
5. (Opt). Install and set the Zsh-Autosuggestions plugin (https://github.com/zsh-users/zsh-autosuggestions​)



