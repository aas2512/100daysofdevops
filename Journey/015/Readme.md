
# AWS EFS and LB / ELK

## DevOps Learning

### AWS EFS - Elastic File Storage

#### What is AWS EFS?

Amazon Elastic File System (Amazon EFS) provides a scalable, fully managed elastic distributed file system based on NFS. It is persistent file storage & can be easily scaled up to petabytes.

It is designed to share parallelly with thousands of EC2 instances to provide better throughput and IOPS. It is a regional service automatically replicated across multiple AZ’s to provide High Availability and durability.

Types of EFS Storage Classes:<br>
- Standard Storage For frequently accessed files.<br>
- Infrequent Access Storage( EFS-IA ) For files not accessed every day Cost-Optimized (costs only $0.025/GB-month)<br>
Use EFS Lifecycle Management to move the file to EFS IA<br>

EFS Access Modes :<br>

1) Performance Modes:<br>
● General Purpose: low latency at the cost of lower throughput.<br>
● Max I/O: high throughput at the cost of higher latency.<br>

2) Throughput Modes :<br>
● Bursting (default): throughput grows as the file system grows<br>
● Provisioned: specify throughput in advance. (fixed capacity)<br>

Features:<br>
● Fully Managed and Scalable, Durable, Distributed File System (NFSv4)<br>
● Highly Available & Consistent low latencies. (EFS is based on SSD volumes)<br>
● POSIX Compliant (NFS) Distributed File System.<br>
● EC2 instances can access EFS across AZs, regions, VPCs & on-premises through AWS Direct Connect or AWS VPN.<br>
● Provides EFS Lifecycle Management for the better price-performance ratio<br>
● It can be integrated with AWS Datasync for moving data between on-premise to AWS EFS<br>
● Supported Automatic/Schedule Backups of EFS (AWS Backups)<br>
● It can be integrated with CloudWatch & CloudTrail for monitoring and tracking.<br>
● EFS supports encryption at transit(TLS) and rest both. (AWS Key Management Service (KMS))<br>
● Different Access Modes: Performance and Throughput for the better cost-performance tradeoff.<br>
● EFS is more expensive than EBS.<br>
● Once your file system is created, you cannot change the performance mode<br>
● Not suitable for boot volume & highly transactional data (SQL/NoSQLdatabases)<br>
● Read-after-write consistency for data access.<br>
● Integrated with IAM for access rights & security.<br>

Use Cases: (Sharing Files Across instances/containers)<br>
● Mission critical business applications<br>
● Microservice based Applications<br>
● Container storage<br>
● Web serving and content management<br>
● Media and entertainment file storage<br>
● Database Backups<br>
● Analytics and Machine Learning<br>

Best Practices:<br>
● Monitor using cloudWatch and track API using CloudTrails<br>
● Leverage IAM services for access rights and security<br>
● Test before fully migrating mission critical workload for performance andthroughput.<br>
● Separate out your latency-sensitive workloads. Storing these workloads on separate volumes ensures dedicated I/O and burst capabilities.

Pricing:<br>
● Pay for what you have used based on Access Mode/Storage Type + Backup Storage.



### AWS LOAD BALANCER

What is AWS Elastic Load Balancer?

● ELB Stands for Elastic Load Balancer.<br>
● It distributes the incoming traffic to multiple targets such as Instances, Containers, Lambda Functions, IP Addresses etc.<br>
● It spans in single or multiple availability zones.<br>
● It provides high availability, scaling and security for the application.<br>

Types of Elastic Load Balancer

Application Load Balancer<br>
o It is best suited for load balancing of the web applications and websites.<br>
o It routes traffic to targets within Amazon VPC based on the content of the request.<br>

Network Load Balancer<br>
o It is mostly for the application which has ultra-high performance.<br>
o This load balancer also acts as a single point of contact for the clients.<br>
o This Load Balancer distributes the incoming traffic to the multiple targets.<br>
o The listener checks the connection request from the clients using the protocol and ports we specify.<br>
o It supports TCP, UDP and TLS protocol.

Gateway Load Balancer (Newly Introduced)<br>
● It is like other load balancers but it is for third-party appliances.<br>
● This provides load balancing and auto scaling for the fleet of third-party appliances.<br>
● It is used for security, network analytics and similar use cases.<br>

Classic Load Balancer<br>
● It operates at request and connection level.<br>
● It is for the EC2 Instance build in the old Classic Network.<br>
● It is an old generation Load Balancer.<br>
● AWS recommends to use Application or Network Load Balancer instead.<br>

Listeners<br>
● A listener is a process that checks for connection requests, using the protocol and port that you configured.<br>
● You can add HTTP, HTTPS or both.

Target Group<br>
● It is the destination of the ELB.<br>
● Different target groups can be created for different types of requests.<br>
● For example, one target group i.e., a fleet of instances will be handling the general request and other target groups will handle the other type of request such as micro services.<br>
● Currently, three types of target supported by ELB: Instance, IP and Lambda Functions.

Health Check<br>
● Health checks will be checking the health of Targets regularly and if any target is unhealthy then traffic will not be sent to that Target.<br>
● We can define the number of consecutive health checks failure then only the Load Balancer will not send the traffic to those Targets.<br>
● e.g., If 4 EC2 are registered as Target behind Application Load Balancer and if one of the EC2 Instance is not healthy then Load Balancer will not send the traffic to that EC2 Instance

Use Cases:<br>
● Web Application Deployed in Multiple Servers: If a web Application/Website is deployed in multiple EC2 Instances then we can distribute the traffic between the  Application Load Balancers.<br>
● Building a Hybrid Cloud: Elastic Load Balancing offers the ability to load balance across AWS and on-premises resources, using a single load balancer. You can achieve this by registering all of your resources to the same target group and associating the target group with a load balancer.<br>
● Migrating to AWS: ELB supports the load balancing capabilities critical for you to migrate to AWS. ELB is well positioned to load balance both traditional as well  as cloud native applications with auto scaling capabilities that eliminate the guess work in capacity planning.<br>

Charges:<br>
● Charges will be based on each hour or partial hour that the ELB is running.<br>
● Charges will also depend on the LCU (Load Balancer Units)


## Mídia
#### Mapa Mental  

#### RDS
<p><center>
<img src="ELB_MindMap.jpg"></center></p>