
# MONITORAMENTO COM PROMETHEUS E GRAFANA

## DevOps Learning

- ✍️ Estudo e prática de como instalar, configurar e usar tanto o Prometheus quanto o Grafana, reforçando os conceitos fundamentais dessas tecnologias. Criar as métricas, dashboards e alertas que melhor se adequem à sua realidade.

- ✍️ Monitorar uma aplicação web e um servidor linux. 

- ✍️ O que é Prometheus?

- ✍️ Desenvolvido originalmente em 2012 pela SoundCloud, o Prometheus é atualmente uma ferramenta open-source utilizada para o monitoramento em tempo real de milhares de métricas da sua aplicação. Em 2016, o Prometheus tornou-se um projeto graduado na Cloud Native Computing Foundation, assim como o Kubernetes.

- ✍️ O que é Grafana?

- ✍️ O Grafana é uma aplicação para criação de gráficos e dashboards incríveis, utilizando como fonte de dados o Prometheus (dentre outras possibilidades). Com cadastramento de usuários e permissões, o Grafana é o lugar perfeito para visualizar em tempo real o que está acontecendo com a sua aplicação ou seu ambiente.

#### PROMETHEUS 
```
docker run -d \
    -p 9090:9090 \
    -v <path>/prometheus.yml:/etc/prometheus/prometheus.yml \
    prom/prometheus
```

#### GRAFANA
```
docker run -d --name=grafana -p 3000:3000 grafana/grafana
```

#### APP NODE
```
cd app
npm install
node .index.js
```

#### NODE EXPORTER LINUX
```
wget https://github.com/prometheus/node_exporter/releases/download/v*/node_exporter-*.*-amd64.tar.gz
tar xvfz node_exporter-*.*-amd64.tar.gz
cd node_exporter-*.*-amd64
./node_exporter
```

endpoints:<br>
<ip/localhost>:3030/<br>
<ip/localhost>:3030/metrics<br>
<ip/localhost>:3030/zera-usuarios-logados<br>
<ip/localhost>:3030/retorna-usuarios-logados<br>


## Midia
#### DASHBOARD APP
<p><center>
<img src="dash_app_aula.png"></center></p>

#### DASHBOARD LINUX
<p><center>
<img src="dash_linux.png"></center></p>


