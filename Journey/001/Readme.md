
# LINUX/K8S/TERRAFORM

## DevOps Learning

- ✍️ RHSA II - Chapter 6: Gerenciamento de armazenamento básico
- ✍️ K8s - DaemonSet, StaticPod, Multiple Scheduler
- ✍️ TERRAFORM


# TERRAFORM

### TERRAFORM INSTALL
I installed on Debian

`curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -`

`sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"`

`sudo apt install terraform`

You need:
 - The Terraform CLI installed.
 - The AWS CLI installed.
 - An AWS account.
 - Create credentials account on AWS.
 - Configure AWS CLI;

 - Create a directory named learn-terraform-aws-instance and paste the following configuration into a file named main.tf.

 ```
 terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.35"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = "us-west-2"
}

resource "aws_instance" "example" {
  ami           = "ami-830c94e3"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleInstance"
  }
}
```

`$ terraform init`

`$ terraform apply`

Terraform configurations can include variables to make your configuration more dynamic and flexible.
Create a new file called variables.tf with a block defining a new instance_name variable.
```
variable "instance_name" {
  description = "Value of the Name tag for the EC2 instance"
  type        = string
  default     = "ExampleInstance"
}
```
##Output EC2 instance configuration
Create a file called outputs.tf in your learn-terraform-aws-instance directory.

Add outputs to the new file for your EC2 instance's ID and IP address.

```
output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.example.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.example.public_ip
}
```
