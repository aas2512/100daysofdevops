#ACTIVE AWS PROVIDERS
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.35"
    }
  }
}
#ACTIVE AWS PROVIDERS
provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

#CREATE CUSTOM VPC
resource "aws_vpc" "technical-test-vpc" {
  cidr_block = var.vpc_cidr_blocks
  tags = {
    Name : "${var.env_prefix}-vpc"
  }
}

#CREATE SUBNET ON CUSTOM VPC
resource "aws_subnet" "technical-test-subnet-1" {
  vpc_id            = aws_vpc.technical-test-vpc.id
  cidr_block        = var.subnet_cidr_blocks
  availability_zone = var.avail_zone

  tags = {
    Name : "${var.env_prefix}-subnet-1"
  }
}

#CREATE THE IG TO CUSTOM VPC
resource "aws_internet_gateway" "technical-test-igw" {
  vpc_id = aws_vpc.technical-test-vpc.id

  tags = {
    Name : "${var.env_prefix}-igw"
  }
}
//ASSOCIATE THE IGW TO DEFAULT ROUTE TABLE CREATED
resource "aws_default_route_table" "technical-test-main-rtb" {
  default_route_table_id = aws_vpc.technical-test-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.technical-test-igw.id
  }
  tags = {
    Name : "${var.env_prefix}-main-rtb"
  }
}

resource "aws_default_security_group" "technical-test-dmz-sg" {
  vpc_id = aws_vpc.technical-test-vpc.id

  ingress {
    description = "SSH Access"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Web Access from Jenkins"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Web Access from Nginx"
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "All ports"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }

  tags = {
    Name : "${var.env_prefix}-default-dmz-sg"
  }
}

############################################################
# SSH KEY FOR INSTANCE ACCESS
############################################################
resource "aws_key_pair" "technical-test_ssh_key" {
  key_name   = "server-key"
  public_key = file(var.public_key_location)
}


############################################################
# INSTANCE FOR JENKINS
############################################################
resource "aws_instance" "technical-test-jenkins" {

  //REQUIRED ARGUMENTS
  ami           = data.aws_ami.technical-test_latest_ami_linux_image.id
  instance_type = var.instance_type_dev_ops

  //OPTIONAL ARGUMENTS
  subnet_id = aws_subnet.technical-test-subnet-1.id
  vpc_security_group_ids = [
    aws_default_security_group.technical-test-dmz-sg.id
  ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true

  key_name = aws_key_pair.technical-test_ssh_key.key_name

  user_data = file("entry-script-dev-ops.sh")
  

  tags = {
    Name : "${var.env_prefix}-ec2-dev-ops"
  }
}

############################################################
# INSTANCE CREATE DEPLOY APP
############################################################
resource "aws_instance" "technical-test-deploy-app" {

  //REQUIRED ARGUMENTS
  ami           = data.aws_ami.technical-test_latest_ami_linux_image.id
  instance_type = var.instance_type_dev_deploy

  //OPTIONAL ARGUMENTS
  subnet_id = aws_subnet.technical-test-subnet-1.id
  vpc_security_group_ids = [
    aws_default_security_group.technical-test-dmz-sg.id
  ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true

  key_name = aws_key_pair.technical-test_ssh_key.key_name

  user_data = file("entry-script-dev-deploy.sh")
  

  tags = {
    Name : "${var.env_prefix}-ec2-dev-deploy"
  }
}

############################################################
# GET DATA AMI IMAGES FROM AWS
############################################################
data "aws_ami" "technical-test_latest_ami_linux_image" {
  #get the image most recent
  most_recent = true

  #pass to filter to get images from this owner
  owners = [
    "amazon"
  ]

  #fetch the data from AWS with the parameters below
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
