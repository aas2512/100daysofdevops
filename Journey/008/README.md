
# LINUX / KUBERNETES

## DevOps Learning

1. LINUX: Managing Network Security

----

### 1.Linux
#### Managing Network Security

Linux kernel includes netfilter, a framework for network traffic operations such as packet filtering, network address translation and port translation. 

Linux kernel also includes nftables, a new filter and packet classification subsystem that has enhanced portions of netfilter's code, but retaining the netfilter architecture such as networking stack hooks, connection tracking system, and the logging facility.

Firewalld is a dynamic firewall manager, a front end to the nftables framework using the nft command. Until the introduction of nftables, firewalld used the iptables command to configure netfilter directly, as an improved alternative to the iptables service. In RHEL 8, firewalld remains the recommended front end, managing firewall rulesets using nft. 

**Pre-defined Zones**

Firewalld has pre-defined zones, each of which you can customize.

<p><center>
<img src="firewall-pre-define-zones.png"></center></p>

**Pre-defined Services**

Firewalld has a number of pre-defined services.

<p><center>
<img src="firewall-pre-define-services.png"></center></p>

There three way to configure firewall:
1. Directly in /etc/firewalld/
2. Web Console
3. firewall-cmd command line tool


**Configuring the Firewall from the Command Line**

```
[root@host ~]# firewall-cmd --set-default-zone=dmz
[root@host ~]# firewall-cmd --permanent --zone=internal \
--add-source=192.168.0.0/24
[root@host ~]# firewall-cmd --permanent --zone=internal --add-service=mysql
[root@host ~]# firewall-cmd --reload
```

<p><center>
<img src="firewall-cmd-commands.png"></center></p>

----

