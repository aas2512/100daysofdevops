#this file I declare the variables
#you need create the terraform.tfvars and pass the key=value; 
#Ex: vpc_cidr_blocks="10.0.0.0/16"

variable "aws_access_key" {}
variable "aws_secret_key" {}

variable "region" {
  default = "us-east-1"
}
variable "vpc_cidr_blocks" {}
variable "subnet_cidr_blocks" {}
variable "avail_zone" {}
variable "env_prefix" {}
variable "instance_type_dev_deploy" {}
variable "instance_type_dev_ops" {}
variable "public_key_location"{}