terraform {
  required_providers {
    
    aws = {
      source = "hashicorp/aws"
      version = "3.42.0"
    }
  }

  backend "s3" {
  bucket = "devopsjourney"
  key = "devopslearn/state.tfstate"
  region = "us-east-1"
}
}



provider "aws" {

  region = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key

}

resource "aws_key_pair" "cluster" {
  key_name   = "cluster-key"
  public_key = var.public_ssh_key
}


module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "cluster-k8s"
  instance_count         = 2

  user_data = file("script.sh")

  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.cluster.key_name
  monitoring             = false

  vpc_security_group_ids = [
      module.main_sg.security_group_id
      ]
  subnet_ids              = tolist(module.vpc.public_subnets)

  tags = {
    Terraform   = "true"
    Environment = "${var.enviroment}-devopslearn"
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "vpc-cluster-k8s"
  cidr = var.cidr_block

  azs             = ["us-east-1a","us-east-1b","us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    Environment = "${var.enviroment}-devopslearn"
  }
}

#############################################################
# Security group which is used as an argument in complete-sg
#############################################################
module "main_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "main_sg"
  description = "Security group which is used as an argument in complete-sg"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["https-443-tcp", "ssh-tcp", "http-8080-tcp"]

  egress_cidr_blocks = ["0.0.0.0/0"]
  egress_rules = ["all-all"]
}


data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}