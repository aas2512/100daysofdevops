#TERRAFORM CLOUD CONFIGURATION
/* terraform {
  backend "remote" {
    organization = "devops-learn-alan"

    workspaces {
      name = "aws-basic-infrastructure"
    }
  }
} */

#ACTIVE AWS PROVIDERS
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.35"
    }
  }
}

#INITIAL CONFIGURATION OF AWS PROVIDERS
provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

#CREATE CUSTOM VPC
resource "aws_vpc" "devopslearn-vpc" {
  cidr_block = var.vpc_cidr_blocks
  tags = {
    Name : "${var.env_prefix}-vpc"
  }
}

#CREATE SUBNET ON CUSTOM VPC
resource "aws_subnet" "devopslearn-subnet-1" {
  vpc_id            = aws_vpc.devopslearn-vpc.id
  cidr_block        = var.subnet_cidr_blocks
  availability_zone = var.avail_zone

  tags = {
    Name : "${var.env_prefix}-subnet-1"
  }
}

#CREATE THE IG TO CUSTOM VPC
resource "aws_internet_gateway" "devopslearn-igw" {
  vpc_id = aws_vpc.devopslearn-vpc.id

  tags = {
    Name : "${var.env_prefix}-igw"
  }
}
//ASSOCIATE THE IGW TO DEFAULT ROUTE TABLE CREATED
resource "aws_default_route_table" "devopslearn-main-rtb" {
  default_route_table_id = aws_vpc.devopslearn-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.devopslearn-igw.id
  }
  tags = {
    Name : "${var.env_prefix}-main-rtb"
  }
}

resource "aws_default_security_group" "devopslearn-dmz-sg" {
  vpc_id = aws_vpc.devopslearn-vpc.id

  ingress {
    description = "SSH Access"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Web Access from Nginx"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "All ports"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }

  tags = {
    Name : "${var.env_prefix}-default-dmz-sg"
  }
}

############################################################
# SSH KEY FOR INSTANCE ACCESS
############################################################
resource "aws_key_pair" "devopslearn_ssh_key" {
  key_name   = "server-key"
  public_key = file(var.public_key_location)
  //public_key = var.ssh_public_key
}

############################################################
# INSTANCE CREATE
############################################################
resource "aws_instance" "devopslearn-server" {

  //REQUIRED ARGUMENTS
  ami           = data.aws_ami.devopslearn_latest_ami_linux_image.id
  instance_type = var.instance_type

  //OPTIONAL ARGUMENTS
  subnet_id = aws_subnet.devopslearn-subnet-1.id
  vpc_security_group_ids = [
    aws_default_security_group.devopslearn-dmz-sg.id
  ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true

  key_name = aws_key_pair.devopslearn_ssh_key.key_name

  user_data = file("entry-script.sh")
  
  //PROVISIONERS IS THE LAST RESORT TO USE ON TERRAFORM
  //MAKE CONNECTION WITH THE CREATED INSTANCE
  /* connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = file(var.private_key_location)
  } */

  //GET THE FILE AND SEND TO INSTANCE
 /*  provisioner "file" {
    source = "entry-script.sh"
    destination = "/home/ec2-user/entry-script-on-ec2.sh"
  } */

  //EXECUTE THE SENDED SCRIPT
  /* provisioner "remote-exec" {
    script = file("entry-script-on-ec2.sh")
  } */

  //EXECUTE COMMAND LOCALY
  /* provisioner "local-exec" {
    command = "echo ${self.public_ip} > output.txt"
  } */

  tags = {
    Name : "${var.env_prefix}-ec2-instance"
  }
}

############################################################
# GET DATA AMI IMAGES FROM AWS
############################################################
data "aws_ami" "devopslearn_latest_ami_linux_image" {
  #get the image most recent
  most_recent = true

  #pass to filter to get images from this owner
  owners = [
    "amazon"
  ]

  #fetch the data from AWS with the parameters below
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}
