
# DAY 05
# k8s / Terraform / Linux

## DevOps Learning

1. Kubernetes: Secrets / multi-container PODs / InitContainers
2. Linux: Implementing Advanced Storage Features
   - Straits
   - Compressing and Deduplicating Storage with VDO

### 1.Kubernetes
#### Secrets

Kubernetes Secrets let you store and manage sensitive information, such as passwords, OAuth tokens, and ssh keys.

*Kubernetes Secrets are, by default, stored as unencrypted base64-encoded strings. By default they can be retrieved - as plain text - by anyone with API access, or anyone with access to Kubernetes' underlying data store, etcd.*

Secrets are not encrypted, so it is not safer in that sense. However, some best practices around using secrets make it safer. As in best practices like:<br/>
- Not checking-in secret object definition files to source code repositories.<br/>
- Enabling Encryption at Rest for Secrets so they are stored encrypted in ETCD. 

**Create a Secret**<br/>
```
kubectl create secret generic db-user-pass \
  --from-file=username=./username.txt \
  --from-file=password=./password.txt
```

```
kubectl create secret generic dev-db-secret \
  --from-literal=username=devuser \
  --from-literal=password='S!B\*d$zDsb='
```

Create yaml secret<br>
```
apiVersion: v1
kind: Secret
metadata:
  name: mysecret
type: Opaque
data:
  USER_NAME: YWRtaW4=
  PASSWORD: MWYyZDFlMmU2N2Rm
```
Apply: `kubectl apply -f mysecret.yaml`

Use envFrom to define all of the Secret's data as container environment variables. The key from the Secret becomes the environment variable name in the Pod.

```
apiVersion: v1
kind: Pod
metadata:
  name: secret-test-pod
spec:
  containers:
    - name: test-container
      image: k8s.gcr.io/busybox
      command: [ "/bin/sh", "-c", "env" ]
      envFrom:
      - secretRef:
          name: mysecret
  restartPolicy: Never
```

#### Multi-container
In a multi-container pod, each container is expected to run a process that stays alive as long as the POD's lifecycle. For example in the multi-container pod that we talked about earlier that has a web application and logging agent, both the containers are expected to stay alive at all times. The process running in the log agent container is expected to stay alive as long as the web application is running. If any of them fails, the POD restarts.

#### InitContainers

A task that will be run only one time when the pod is first created.<br>
**initContainer** is configured in a pod like all other containers, except that it is specified inside a initContainers section

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers:
  - name: init-myservice
    image: busybox
    command: ['sh', '-c', 'git clone <some-repository-that-will-be-used-by-application> ; done;']
```

*You can configure multiple such initContainers as well, like how we did for multi-pod containers. In that case each init container is run one at a time in sequential order.*

----

### 2. Linux
#### Straits

Stratis is a *new local storage-management solution* for Linux. Stratis is designed to make it easier to perform initial configuration of storage, make changes to the storage configuration, and use advanced storage features.

Stratis runs as a service that manages **pools of physical storage devices** and transparently creates and manages volumes for the newly created file systems.

Diagram illustrates how the elements of the Stratis storage management solution are assembled. Block storage devices such as hard disks or SSDs are assigned to pools, each contributing some physical storage to the pool. File systems are created from the pools, and physical storage is mapped to each file system as it is needed.

<p align="center">
  <img src="storage-modern.svg">
</p>

Create pools of one or more block devices using the stratis pool create command.<br>
`[root@host ~]# stratis pool create pool1 /dev/vdb`<br>
*Each pool is a subdirectory under the /stratis directory.*

Use the stratis pool list command to view the list of available pools.<br> 
```
[root@host ~]# stratis pool list
Name     Total Physical Size  Total Physical Used
pool1                  5 GiB               52 MiB
```

Use the stratis pool add-data command to add additional block devices to a pool.
`[root@host ~]# stratis pool add-data pool1 /dev/vdc`

Use the stratis blockdev list command to view the block devices of a pool.<br>
```
[root@host ~]# stratis blockdev list pool1
Pool Name  Device Node    Physical Size   State  Tier
pool1      /dev/vdb               5 GiB  In-use  Data
pool1      /dev/vdc               5 GiB  In-use  Data
```

Use the stratis filesystem create command to create a file system from a pool.<br>
`[root@host ~]# stratis filesystem create pool1 fs1`
The links to the Stratis file systems are in the /stratis/pool1 directory.

Use the stratis filesystem list command to view the list of available file systems.<br>
```
[root@host ~]# stratis filesystem list
Pool Name  Name  Used     Created            Device              UUID
pool1      fs1   546 MiB  Sep 23 2020 13:11  /stratis/pool1/fs1  31b9363badd...
```

You can create a snapshot of a Stratis-managed file system:
`[root@host ~]# stratis filesystem snapshot pool1 fs1 snapshot1`

Persistently Mounting Stratis File Systems
```
[root@host ~]# lsblk --output=UUID /stratis/pool1/fs1
UUID
31b9363b-add8-4b46-a4bf-c199cd478c55
```

/etc/fstab file to persistently mount a Stratis file system
`UUID=31b9363b-add8-4b46-a4bf-c199cd478c55 /dir1 xfs defaults,x-systemd.requires=stratisd.service 0 0`<br>
***If you do not include the x-systemd.requires=stratisd.service mount option in /etc/fstab for each Stratis file system, the machine will fail to start properly and will abort to emergency.target the next time it is rebooted.***


#### Compressing and Deduplicating Storage with VDO

RHEL 8 includes the Virtual Data Optimizer (VDO) driver, which optimizes the data footprint on block devices. VDO is a Linux device mapper driver that reduces disk space usage on block devices, and minimizes the replication of data, saving disk space and even increasing data throughput. VDO includes two kernel modules: the kvdo module to transparently control data compression, and the uds module for deduplication.

VDO applies three phases:
- Zero-Block Elimination 
- Deduplication
- Compression

Install the vdo and kmod-kvdo packages to enable VDO in the system.<br/>
`[root@host ~]# yum install vdo kmod-kvdo -y`

To create a VDO volume, run the vdo create command.<br/>
`[root@host ~]# vdo create --name=vdo1 --device=/dev/vdd --vdoLogicalSize=50G`

Analyzing a VDO Volume<br/>
`[root@host ~]# vdo status --name=vdo1`