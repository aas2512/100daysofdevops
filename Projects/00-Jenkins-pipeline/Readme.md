
# CONSTRUÇÃO PIPELINE USANDO JENKINS, ANSIBLE, DOCKER, EC2

## DevOps Learning

A construção desse pipeline foi a partir de um desafio proposto no curso Formação DevOps Cloud da Cloud Treinamentos.

O desafio era montar um pipeline no qual a partir de um commit no gitlab disparesse a esteira no jenkins, finalizando com um docker push no hub. Fui um pouco além e adicionei mais algumas funcionalidades.

- Criação de duas instâncias EC2;
- Instalação e configuração do Jenkins/Maven/Ansible e demais dependencias;
- Criação do Dockerfile;

O Passo a passo do pipeline ao receber a notificação através do webhook:
- Baixa o repositorio do #gitlab;
- Compila com o Maven;
- Builda a imagem docker com o package;
- Faz docker push;
- Executa um playbook com #Ansible para criar a estrutura na instância(instala python, docker-py, docker, e faz o deploy da imagem no docker)
- Manda noticações do pipeline diretamente para o Slack com o status do pipeline;

[Link do Repositório](https://gitlab.com/aas2512/jenkins-ansible-docker)
<!-- blank line -->
<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="Projects/00-Jenkins-pipeline.png">
    <source src="Projects/00-Jenkins-pipeline.mp4" type="video/mp4">
  </video>
</figure>
<!-- blank line -->
