# Projeto IA

### Ferramentas
- Terraform
- Jenkins
- Ansible
- Github
- Grafana/Prometheus

## ETAPAS
1. Provisionar infraestrutura(EC2, VPC Custom) na AWS com o Terraform;
2. Instalação das ferramentas: Docker, Jenkins, Prometheus e Grafana na instancia dev-ops(t2.medium)
3. Instalação da ferramenta Docker e deploy da aplicação na instancia dev-deploy(t2.micro);



### ETAPA BUILD E PROVISIONAMENTO

1. Fazer o build do Jenkins:
```
cd jenkins
docker build -t <user>/jenkins-lts-debian:v1 .
docke push <user>/jenkins-lts-debian:v1
```

Na pasta terraform alterar a imagem do container no entry-script-dev-ops.sh e colocar a que foi buildada logo acima.

2. Preencher as variaveis necessárias no terraform.auto.tfvars, validar o codigo e fazer um plan:

```
cd terraform
terraform init
terraform validate
terraform plan --out=lab
terraform apply lab

```

Dentro do script 'entry-script-dev-ops.sh' tem os comandos para instalar o docker, git, docker-compose e subir o jenkins em container; 

PS: Apos acessar o jenkins adicionar as credentiais do docker para que possa fazer o push das imagens.

### ANSIBLE
Adicionar uma crendencial no jenkins do tipo ssh username and private key para que o ansible possa fazer a comunicação com os targets;
nome da chave: dev-server
user: ec2-user

Alterar o arquivo dentro da pasta ansible/inventory.inv e colocar o ip interno da maquina deploy_app

<br><br><br>

#

# BUG Ansible /AMZN Linux
Para que o playbook rode
https://bleepcoder.com/ansible/445428804/ansible-docker-and-docker-compose-issue-on-redhat-7-x

# Rodar no TARGET
```
amazon-linux-extras install epel
amazon-linux-extras install ansible2
yum install python-pip
mv /usr/lib/python2.7/site-packages/ansible /usr/lib/python2.7/site-packages/ansible.bak 
sudo yum install python-pip
sudo pip install docker
```









# Technical Test DevOps


Description:


Create the infrastructure to deploy a simple app.

Feel free to use any application here but this app should use at least mysql as database;


Run the app with Docker

Build a CI/CD using Jenkins pipeline. Add a Jenkinsfile to the application that can be used in a Jenkins environment and create a docker image with Jenkins that we can use to test your pipeline(Gitlab, github actions or other similar could be used if you are not comfortable with Jenkins pipeline);

Write documentation on how to run app clearly and objectively; If possible automate to run everything in single and simple commands


Plus:

Container orchestration

Provisioning the infrastructure you needed to accomplish this challenge using terraform or ansible

Deploy the app in this infrastructure;

Add monitoring with Prometheus and Grafana

Secret Management with Vault


Rules:

- Codes must be published on Github

- 5 days for delivery this challenge
#