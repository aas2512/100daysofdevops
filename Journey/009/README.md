
# LINUX 

## DevOps Learning

1. Linux: Installing Red Hat Enterprise Linux


### Linux
#### Installing RHE

**Troubleshooting the Installation**

Anaconda provides two virtual consoles. The first has five windows provided by the tmux software terminal multiplexer. You can access that console using *Ctrl+Alt+F1*. The second virtual console, which displays by default, shows the Anaconda graphical interface. You can access it using *Ctrl+Alt+F6*.



**Automating Installation with Kickstart**

You can automate the installation of Red Hat Enterprise Linux using a feature called Kickstart. Using Kickstart, you specify everything Anaconda needs to complete an installation, including disk partitioning, network interface configuration, package selection, and other parameters, in a Kickstart text file. By referencing the text file, Anaconda performs the installation without further user interaction. 

**Installing and Configuring Virtual Machines**

Virtualization is a feature that allows a single physical machine to be divided into multiple virtual machines (VM), each of which can run an independent operating system. 

In Red Hat Enterprise Linux, manage KVM with the **virsh** command or with Cockpit's Virtual Machines tool. 
 
<p><center>
<img src="configure-kvm.svg"></center></p>
