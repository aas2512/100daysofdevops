
# LINUX / ANSIBLE

## DevOps Learning

1. LINUX: Controlling the boot process

----

### 1.Linux
#### Controlling the boot process


**Selecting the Boot Target**

Commonly Used Targets

Target	                Purpose<br>
**graphical.target**	System supports multiple users, graphical- and text-based logins.<br>
**multi-user.target**	System supports multiple users, text-based logins only.<br>
**rescue.target**	    sulogin prompt, basic system initialization completed.<br>
**emergency.target**	sulogin prompt, initramfs pivot complete, and system root mounted on / read only.

```
[user@host ~]$ systemctl list-units --type=target --all
  UNIT                      LOAD      ACTIVE   SUB    DESCRIPTION
  ---------------------------------------------------------------------------
  basic.target              loaded    active   active Basic System
  cryptsetup.target         loaded    active   active Local Encrypted Volumes
  emergency.target          loaded    inactive dead   Emergency Mode
  getty-pre.target          loaded    inactive dead   Login Prompts (Pre)
  getty.target              loaded    active   active Login Prompts
  graphical.target          loaded    inactive dead   Graphical Interface
...output omitted...
```

```
[root@host ~]# systemctl get-default
multi-user.target
```

`[root@host ~]# systemctl set-default TARGET`

**Resetting the Root Password**

On Red Hat Enterprise Linux 8, it is possible to have the scripts that run from the initramfs pause at certain points, provide a root shell, and then continue when that shell exits. This is mostly meant for debugging, but you can also use this method to reset a lost root password.

To access that root shell, follow these steps:<br>
    1. Reboot the system.<br>
    2. Interrupt the boot loader countdown by pressing any key, except Enter.<br>
    3. Move the cursor to the kernel entry to boot.<br>
    4. Press e to edit the selected entry.<br>
    5. Move the cursor to the kernel command line (the line that starts with linux).<br>
    6. Append rd.break. With that option, the system breaks just before the system hands control from the initramfs to the actual system.<br>
    7. Press Ctrl+x to boot with the changes.

To reset the root password from this point, use the following procedure:

1. Remount /sysroot as read/write.<br>
`switch_root:/# mount -o remount,rw /sysroot`

2. Switch into a chroot jail, where /sysroot is treated as the root of the file-system tree.<br>
`switch_root:/# chroot /sysroot`

3. Set a new root password.<br>
`sh-4.4# passwd root`

4. Make sure that all unlabeled files, including /etc/shadow at this point, get relabeled during boot.<br>
`sh-4.4# touch /.autorelabel`

5. Type exit twice. The first command exits the chroot jail, and the second command exits the initramfs debug shell.

**Repairing Systemd Boot Issues**

To troubleshoot service startup issues at boot time, Red Hat Enterprise Linux 8 makes the following tools available.

***Enabling the Early Debug Shell***

By enabling the debug-shell service with systemctl enable debug-shell.service, the system spawns a root shell on TTY9 (Ctrl+Alt+F9) early during the boot sequence. This shell is automatically logged in as root, so that administrators can debug the system while the operating system is still booting.

**Resume**

systemctl reboot and systemctl poweroff reboot and power down a system, respectively.

systemctl isolate target-name.target switches to a new target at runtime.

systemctl get-default and systemctl set-default can be used to query and set the default target.

Use rd.break on the kernel command line to interrupt the boot process before control is handed over from the initramfs. The root file system is mounted read-only under /sysroot.

The emergency target can be used to diagnose and fix file-system issues

----
