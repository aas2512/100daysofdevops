#!/bin/bash
yum update -y &&  yum install -y docker
systemctl start docker
usermod -aG docker ec2-user
chkconfig docker on
curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
curl https://gist.githubusercontent.com/alanabreudotdev/cf09bc490ace5f35837a030eb2756fe4/raw/a0436b81db2975aaf83773d9821223579d25328f/jenkins-docker-compose.yml --output docker-compose.yaml
docker-compose up -d
