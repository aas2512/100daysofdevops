############################################################
# INSTANCE CREATE
############################################################
resource "aws_instance" "devopslearn-server" {

  //REQUIRED ARGUMENTS
  ami           = data.aws_ami.devopslearn_latest_ami_linux_image.id
  instance_type = var.instance_type

  //OPTIONAL ARGUMENTS
  subnet_id = var.subnet_id
  vpc_security_group_ids = [
      var.default_security_group_id
  ]
  availability_zone = var.avail_zone

  associate_public_ip_address = true

  key_name = aws_key_pair.devopslearn_ssh_key.key_name

  user_data = file("entry-script.sh")
  
  //PROVISIONERS IS THE LAST RESORT TO USE ON TERRAFORM
  //MAKE CONNECTION WITH THE CREATED INSTANCE
  /* connection {
    type = "ssh"
    host = self.public_ip
    user = "ec2-user"
    private_key = file(var.private_key_location)
  } */

  //GET THE FILE AND SEND TO INSTANCE
 /*  provisioner "file" {
    source = "entry-script.sh"
    destination = "/home/ec2-user/entry-script-on-ec2.sh"
  } */

  //EXECUTE THE SENDED SCRIPT
  /* provisioner "remote-exec" {
    script = file("entry-script-on-ec2.sh")
  } */

  //EXECUTE COMMAND LOCALY
  /* provisioner "local-exec" {
    command = "echo ${self.public_ip} > output.txt"
  } */

  tags = {
    Name : "${var.env_prefix}-ec2-instance"
  }
}


############################################################
# SSH KEY FOR INSTANCE ACCESS
############################################################
resource "aws_key_pair" "devopslearn_ssh_key" {
  key_name   = "server-key-module"
  public_key = file(var.public_key_location)
  //public_key = var.ssh_public_key
}



############################################################
# GET DATA AMI IMAGES FROM AWS
############################################################
data "aws_ami" "devopslearn_latest_ami_linux_image" {
  #get the image most recent
  most_recent = true

  #pass to filter to get images from this owner
  owners = [
    "amazon"
  ]

  #fetch the data from AWS with the parameters below
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}