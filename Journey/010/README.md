
# LINUX / KUBERNETES

## DevOps Learning

- Linux: Running Containers


### Linux
#### Running Containers

Containers and Virtual Machines are different in the way they interact with hardware and the underlying operating system.

<img src="virtualization-vs-containers.png">

Containers using core technologies on RH:<br>
- Control Groups (cgroups) for resource management
- Namespaces for process isolation
- SELinux and Seccomp (Secure Computing mode) to enforce security boundaries;

**RUNNING A BASIC CONTAINER**

`[root@host ~]# yum module install container-tools`

```
[user@host ~]$ podman pull registry.access.redhat.com/ubi8/ubi:latest
Trying to pull registry.access.redhat.com/ubi8/ubi:latest...Getting image source signatures
Copying blob 77c58f19bd6e: 70.54 MiB / 70.54 MiB [=========================] 10s
Copying blob 47db82df7f3f: 1.68 KiB / 1.68 KiB [===========================] 10s
Copying config a1f8c9699786: 4.26 KiB / 4.26 KiB [==========================] 0s
Writing manifest to image destination
Storing signatures
a1f8c969978652a6d1b2dfb265ae0c6c346da69000160cd3ecd5f619e26fa9f3
```

```
[user@host ~]$ podman images
REPOSITORY                            TAG      IMAGE ID       CREATED      SIZE
registry.access.redhat.com/ubi8/ubi   latest   a1f8c9699786   5 weeks ago  211 MB
```

```
[user@host ~]$ podman run -it registry.access.redhat.com/ubi8/ubi:latest
[root@8b032455db1a /]# 
```

**Finding and Managing Container Images**

Podman uses a registries.conf file on your host system to get information about the container registries it can use.

```
[user@host ~]$ cat /etc/containers/registries.conf
# This is a system-wide configuration file used to
# keep track of registries for various container backends.
# It adheres to TOML format and does not support recursive
# lists of registries.

# The default location for this configuration file is /etc/containers/registries.conf.

# The only valid categories are: 'registries.search', 'registries.insecure',
# and 'registries.block'.

[registries.search]
registries = ['registry.redhat.io', 'quay.io', 'docker.io']

# If you need to access insecure registries, add the registry's fully-qualified name.
# An insecure registry is one that does not have a valid SSL certificate or only does HTTP.
[registries.insecure]
registries = []

# If you need to block pull access from a registry, uncomment the section below
# and add the registries fully-qualified name.
#
[registries.block]
registries = []
```

Use the podman search command to search container registries for a specific container image. <br>
`[user@host ~]$ podman search <NAME OF IMAGE>`

The following example inspects a container image and returns image information without pulling the image to the local system:<br>

```
[user@host ~]$ skopeo inspect docker://registry.redhat.io/rhel8/python-36
...output omitted...
                "name": "ubi8/python-36",
                "release": "107",
                "summary": "Platform for building and running Python 3.6 applications",
...output omitted...
```

**Attaching Persistent Storage to a Container**

***Preparing the Host Directory***<br>
When you prepare a host directory, you must configure it so that the processes inside the container can access it. Directory configuration involves:<br>
- Configuring the ownership and permissions of the directory. <br>
- Setting the appropriate SELinux context.<br>

Mounting a Volume

`--volume host_dir:container_dir:Z`

With the Z option, Podman automatically applies the SELinux container_file_t context type to the host directory.

For example, to use the /home/user/dbfiles host directory for MariaDB database files as /var/lib/mysql inside the container, use the following command.

```
[user@host ~]$ podman run -d --name mydb -v /home/user/dbfiles:/var/lib/mysql:Z -e MYSQL_USER=user -e MYSQL_PASSWORD=redhat -e MYSQL_DATABASE=inventory registry.redhat.io/rhel8/mariadb-103:1-102
```

**Managing Containers as Services**

Running Systemd Services as a Regular User

When you enable a user service as a non-root user, that service automatically starts when you open your first session through the text or graphical consoles or using SSH. The service stops when you close your last session. This behavior differs from the system services, which start when the system starts and stop when the system shuts down.

However, you can change this default behavior and force your enabled services to start with the server and stop during the shutdown by running the ***loginctl enable-linger*** command. To revert the operation, use the ***loginctl disable-linger*** command. To view the current status, use the ***loginctl show-user*** username command with your user name as parameter.

```
[user@host ~]$ loginctl enable-linger
[user@host ~]$ loginctl show-user user
...output omitted...
Linger=yes
[user@host ~]$ loginctl disable-linger
[user@host ~]$ loginctl show-user user
...output omitted...
Linger=no
```

To control your new user services, use the systemctl command with the --user option. The following example lists the unit files in the ~/.config/systemd/user/ directory, forces systemd to reload its configuration, and then enables and starts the myapp user service.

```
[user@host ~]$ ls ~/.config/systemd/user/
myapp.service
[user@host ~]$ systemctl --user daemon-reload
[user@host ~]$ systemctl --user enable myapp.service
[user@host ~]$ systemctl --user start myapp.service
```

**Managing Containers Using Systemd Services**

```
[user@host ~]$ cd  ~/.config/systemd/user/
[user@host user]$ podman generate systemd --name web --files --new
/home/user/.config/systemd/user/container-web.service
```

The following example shows the start and stop directives in the unit file when you run the podman generate systemd command with the --new option:

```
[user@host ~]$ podman run -d --name web -v /home/user/www:/var/www:Z registry.redhat.io/rhel8/httpd-24:1-105
[user@host ~]$ podman generate systemd --name web --new
...output omitted...
ExecStart=/usr/bin/podman run --conmon-pidfile %t/%n-pid --cidfile %t/%n-cid --cgroups=no-conmon -d --name web -v /home/user/webcontent:/var/www:Z registry.redhat.io/rhel8/httpd-24:1-105 
ExecStop=/usr/bin/podman stop --ignore --cidfile %t/%n-cid -t 10 
ExecStopPost=/usr/bin/podman rm --ignore -f --cidfile %t/%n-cid 
...output omitted...
```

Configuring Containers to Start When the Host Machine Starts

By default, enabled systemd user services start when a user opens the first session, and stop when the user closes the last session. For the user services to start automatically with the server, run the loginctl enable-linger command:<br>
`[user@host ~]$ loginctl enable-linger`

To enable a container to start when the host machine starts, use the systemctl command:<br>
`[user@host ~]$ systemctl --user enable container-web`

To disable the start of a container when the host machine starts, use the systemctl command with the disable option:<br>
`[user@host ~]$ systemctl --user disable container-web`