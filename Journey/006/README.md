
# Linux / Kubernetes

## DevOps Learning

1. Linux: Accessing Network-Attached Storage

### 1.Linux
#### Secrets

**Mounting and Unmounting NFS Shares**<br>
NFS, the Network File System, is an internet standard protocol used by Linux, UNIX, and similar operating systems as their native network file system. It is an open standard, still being actively enhanced, which supports native Linux permissions and file-system features.

```
[user@host ~]$ sudo mkdir mountpoint
[user@host ~]$ sudo mount serverb:/ mountpoint
[user@host ~]$ sudo ls mountpoint
```

Mount point: Use mkdir to create a mount point in a suitable location.<br>
`[user@host ~]$ mkdir -p mountpoint`

Mount temporarily: Mount the NFS share using the mount command:<br>
`[user@host ~]$ sudo mount -t nfs -o rw,sync serverb:/share mountpoint`

Mount persistently: To ensure that the NFS share is mounted at boot time, edit the /etc/fstab file to add the mount entry.<br>

```
[user@host ~]$ sudo vim /etc/fstab
...
serverb:/share  /mountpoint  nfs  rw,soft  0 0
``` 

Then, mount the NFS share:
`[user@host ~]$ sudo mount /mountpoint`


**Mounting NFS Shares with the Automounter**<br>
The automounter is a service (autofs) that automatically mounts NFS shares "on-demand," and will automatically unmount NFS shares when they are no longer being used.

Install autofs<br>
`[user@host ~]$ sudo yum install autofs`

Add a master map file to /etc/auto.master.d. This file identifies the base directory used for mount points and identifies the mapping file used for creating the automounts.<br>
`[user@host ~]$ sudo vim /etc/auto.master.d/demo.autofs`

Add the master map entry, in this case, for indirectly mapped mounts:<br>
`/shares  /etc/auto.demo`

Create the mapping files. Each mapping file identifies the mount point, mount options, and source location to mount for a set of automounts.<br>
`[user@host ~]$ sudo vim /etc/auto.demo`

The mapping file-naming convention is /etc/auto.name, where name reflects the content of the map.<br>
`work  -rw,sync  serverb:/shares/work`

To use directly mapped mount points, the master map file might appear as follows:<br>
`/-  /etc/auto.direct`

The content for the /etc/auto.direct file might appear as follows:<br>
`/mnt/docs  -rw,sync  serverb:/shares/docs`

Indirect Wildcard Maps
`*  -rw,sync  serverb:/shares/&`


### 2. KUBERNETES ###
#### Upgrade Kubernetes ####

I do an upgrade of version. 
In lab I migrate version of kubernetes from 1.18 to 1.19, master and node01.
