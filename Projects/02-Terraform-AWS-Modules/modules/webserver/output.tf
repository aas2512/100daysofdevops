output "ami_image_id" {
  value = data.aws_ami.devopslearn_latest_ami_linux_image.id
}

//GET PUBLIC IP EC2 INSTANCE
output "public_ip_instance" {
  value = aws_instance.devopslearn-server.public_ip
}

output "private_ip" {
  value = aws_instance.devopslearn-server.private_ip
}