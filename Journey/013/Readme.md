
# AWS / Kubernetes

## DevOps Learning

- ✍️ O dia de hoje foi para estudar sobre:

- **AWS Databases:**
    - RDS
    - DynamoDB
    - Redshift
    - Elasticache
    - Aurora
<br/>

- **Kubernetes:**
    - Revisão em alguns pontos no docker;
    - Revisão de alguns elementos de um deploy no k8s;

## Mídia
#### Mapa Mental  

#### RDS
<p><center>
<img src="RDS-Mindmap.png"></center></p>

#### DynamoDB
<p><center>
<img src="DynamoDB-Mindmap.png"></center></p>

#### Aurora
<p><center>
<img src="Aurora-Mindmap.png"></center></p>


