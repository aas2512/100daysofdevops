
## K8S/TERRAFORM 

### DevOps Learning Day

## KUBERNETES

### 1. ROLLING UPDATES AND ROLLBACKS

 Deployment Strategy
 - Rolling Update(default)
   Is update instance by instance. Down one and Up other
 - Recreate
   Destroy all instances and up all. This strategies make the app down for users;

 Can apply update inside the deployment yaml or kubectl edit or using kubectl set image.

#### UPGRADE

#### ROLLBACK

 <p align="center">
  <img src="k8s_rollback.png">
 </p>

#### COMMANDS

CREATE 

   `kubectl create -f deployments-definitions.yml`

GET

   `kubectl get deployments`

UPDATE 

   `kubectl apply -f deployment-definition.yml`

   `kubectl set image deployment/myapp-deployment nginx=nginx:1.9.1`

STATUS 

   `kubectl rollout status deployment/myapp-deployment`

   `kubectl rollout history deployment/myapp-deployment`

ROLLBACK

   `kubectl rollout undo deployment/myapp-deployment`

  
### 2. APPLICATION COMMANDS & ARGUMENTS
   - Docker commands review
     Container run with entrypoint we can append arguments at the entrypoint command.
      ENTRYPOINT ["command"] / CMD ["command", "argument"]
     Case we dont pass argument with entrypoint, run is exit with error per missing parameters.
     Resolve this pass the default argument in CMD. Case not append argument when run container, the default argument is used.
      
    CMD ["argument"]

   `docker run --name ubuntu-sleeper ubuntu-slepper` 
   
   #image have a entrypoint sleep and CMD args 5

   `docker run --name ubuntu-sleeper ubuntu-slepper 10` 
   
   #we changed the default argument on run the container to 10 seconds

   K8S example:
    ```
    apiVersion: v1
    kind: Pod
    metadata:
        name: ubuntu-sleeper-pod
    spec:
        containers:
        - name: ubuntu-sleeper
            image: ubuntu-sleeper
            command: ["sleep2.0"]
            args: ["10"]
    ```

### 3. ENV Variables in k8s

   `docker run -e APP_COLOR=pink simple-webapp-color`
    
K8S example:

        
        apiVersion: v1
        kind: Pod
        metadata:
            name: simple-webapp-color
        spec:
            containers:
            - name: simple-webapp-color
                image: simple-webapp-color
                ports:
                  - containerPort: 8080
                env:
                  - name: APP_COLOR
                    value: pink
        


   We can pass the enviroment values with 3 differents way:
    
1. Plan Key Value
      
      env: 
        - name: APP_COLOR
          value: pink
      
    
2. ConfigMap
```
      env: 
        - name: APP_COLOR
          valueFrom:
            configMapKeyRef:
```

3. Secrets
```
      env: 
        - name: APP_COLOR
          valueFrom:
            secretKeyRef:
```

### 4. ConfigMaps
  
Create ConfigMaps
Example:

```
    APP_COLOR: blue
    APP_MODE: prod
```
 
  #### Imperative Mode

Pass the map line by line(more complicated)

```    
    kubectl create configmap
      <config-name> --from-literal=<key>=<value>
```
    
```  
    kubectl create configmap 
      app-config --from-literal=APP_COLOR=blue \
                 --from-literal=APP_MOD=prod
``` 

or you can pass from file
    
```
    kubectl create configmap
       <config-name> --from-file=<path-to-file>
```

```
    kubectl create configmap \
       app-config --from-file=app_config.properties
 ```

#### Declarative Mode
continue...


## TERRAFORM

Apply without confirmation:

`# terraform apply -auto-aprove`

Destroy resources:

`# terraform destroy`

#### 1. VARIABLES

Pass variables to templates

We have three ways to pass variables to terraform:


```
variables "name-of-variable" {
    description = "Description to var"
}
```
Above we dont define which variable we can pass.
Using the command line:

`$ terraform apply -var "name-of-varible=value"` 
we can pass the values of variable.

We can create the file variables.tf, pass inside the main.tf or we can create the terraform.tfvars. The last way we have to pass only key=value format. Terraform recognize the file and get the values;

terraform.tfvars 
```
aws_instance_name = "MyInstanceAWS"
subnet_cidr_block = "10.0.40.0/24" 
```

When we want separate the enviroments to be create by terraform we can create separately .tfvars: terraform-dev.tfvars, terraform-prod.tfvars, terraform-qa.tfvars

With terraform can't get the variable files automatically, we have to pass the name of file when we go apply:

`$ terraform apply -var-file terraform-dev.tfvars`

Defaults values on variables makes the value optional. Case we dont pass the variable and value, the default value assume and will be use.

```
variables "name-of-variable" {
    description = "Description to var"
    default = "default value to variable"
}
```
Type Constraints of Variables

```
variables "name-of-variable" {
    description = "Description to var"
    default = "default value to variable"
    type = string or list or number...
}
```

```
variables "name-of-variable" {
    description = "Description to var"
    default = "default value to variable"
    type = list(string)
     or
    type = list(object({
        cidr_block = string
        name = string
    }))
}
```
Passing the values for variables:

```
my-variable = ["string", "string"] 
or
my-variable = [
    { atribute = "string", name = "name of resource" }
    { atribute = "string", name = "name of resource" }
]
```

Example Variables:
```
 cidr_blocks = [
    { cidr_block = "10.0.0.0/16", name = "dev-vpc" }
    { cidr_block = "10.0.10.0/24", name = "dev-subnet" }
]
```

Example passing to resource:

```
resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.development-vpc.id
    cidr_block = var.cidr_blocks[1].cidr_block
    availability_zone = "eu-west-3a"

}