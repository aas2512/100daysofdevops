
# AWS / Kubernetes

## DevOps Learning

- ✍️ Estudo sobre o AWS Route 53 e K8S revisão de conceitos e alguns labs

## AWS ROUTE 53

**O que é?**<br/>
É um serviço gerenciador de DNS no qual é uma coleção de regras e registros com o objetivo de ajudar os clientes/usuários entender como alcançar qualquer servidor pelo seu nome de domínio.

#### Route 53 Hosted Zone
É uma coleção de registros para um domínio específico que podem ser gerenciados juntos.<br/>
Tipos de zonas:
 - Publica - determina como o trafico é roteado na internet
 - Privada - determina como o tráfico é roteado dentro da VPC

#### TTL(segundos)
 - é a quantidade de tempo que o DNS resolver irá criar uma informação de cache sobre os registros e assim reduzir a latencia sobre a consulta.
 - um default para o TTL não existe para qualquer registro mas sempre especificar um TTL de 60 segundos ou menos para os clientes ou usuários possam responder rapidamente as mudanças de status health.

#### CNAME vs Alias
 - CNAME: aponta um hostname para outro ho
 - Alias: aponta um hostname para um recurso na AWS

#### Route 53 Routing Policies
**Simple**
  - usado quando necessitamos redirecionar o tráfego para um único recurso;<br/>
  - não tem suporte a health check;<br/>

**Weighted**<br/>
  - parecido com o Simple, mas você pode especificar um peso para cada recurso;<br/>
  - suporta health check;<br/>

**Failover**<br/>
  - se o primeiro recurso cair (baseado no health check), irá ser roteado para um destino secundário;<br/>
  - suporta health check;<br/>
  
**Geo-location**<br/>
  - roteia o tráfego para o servidor mais próximo do usuário que fez a requisição;<br/>

**Geo-proximity**<br/>
  - Ele roteia o tráfego com base na localização dos recursos para a região mais próxima dentro uma área geográfica;<br/>

**Latency based**<br/>
  - roteia o tráfego para o destino com menor latência;<br/>

**Multi-value answer**<br/>
  - Ele distribui as respostas DNS em vários endereços IP.<br/>
  - Se um servidor da web ficar indisponível após um resolver armazenar em cache uma resposta, o usuário pode tentar até oito outros endereços IP da resposta para reduzir tempo de inatividade.<br/>

#### Casos de uso
 - Quando os usuários registram um domínio com o Route 53, ele se torna o servidor DNS confiável para esse domínio e cria uma zona hospedada pública.<br/>
 - Os usuários podem ter seu domínio registrado em uma conta da AWS e o zona hospedada em outra conta AWS.<br/>
 - Para zonas hospedadas privadas, as seguintes configurações de VPC devem ser 'true':<br/>
    ○ enableDnsHostname.<br/>
    ○ enableDnsSupport.<br/>
 - As verificações de saúde podem ser apontadas para:<br/>
    ○ Endpoints (podem ser endereços IP ou nomes de domínio).<br/>
    ○ Status de outras verificações de saúde.<br/>
    ○ Status de um alarme CloudWatch.<br/>
 - Route53 como um Registro: Um gerenciador de nome de domínio é uma organização que gerencia a reserva de nomes de domínio da Internet.<br/>

#### Preço
 - Não há contratos ou quaisquer pagamentos iniciais para usar o Amazon Route 53.<br/>
 - O Route 53 cobra anualmente para cada nome de domínio registrado através do Route 53.<br/>
 - Taxas diferentes são aplicadas para Standard Queries, Latency Based Routing Queries, Geo DNS and Geo Proximity Queries.<br/>

## Mídia
#### Mapa Mental  

#### RDS
<p><center>
<img src="route53_mindmap.png"></center></p>
