output "ami_image_id" {
  value = data.aws_ami.technical-test_latest_ami_linux_image.id
}

//GET PUBLIC IP EC2 INSTANCE
output "public_ip_instance_deploy_app" {
  value = aws_instance.technical-test-deploy-app.public_ip
}

//GET PUBLIC IP EC2 INSTANCE
output "public_ip_instance_jenkins" {
  value = aws_instance.technical-test-jenkins.public_ip
}

output "private_ip_jenkins" {
  value = aws_instance.technical-test-jenkins.private_ip
}

output "private_ip_deploy_app" {
  value = aws_instance.technical-test-deploy-app.private_ip
}

