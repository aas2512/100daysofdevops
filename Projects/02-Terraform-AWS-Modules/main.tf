#TERRAFORM CLOUD CONFIGURATION
/* terraform {
  backend "remote" {
    organization = "devops-learn-alan"

    workspaces {
      name = "aws-basic-infrastructure"
    }
  }
} */

#ACTIVE AWS PROVIDERS
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.35"
    }
  }
}

#INITIAL CONFIGURATION OF AWS PROVIDERS
provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

#MODULE CUSTOM VPC
module "devopslearn-vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.env_prefix}-devops_vpc"
  cidr = var.vpc_cidr_blocks

  azs             = [var.avail_zone]
  public_subnets  = [var.subnet_cidr_blocks]
  public_subnet_tags = { Name = "${var.env_prefix}-subnet-1"}

  
  tags = {
    Name = "${var.env_prefix}-devops_vpc"
    Terraform = "true"
    Environment = "dev"
  }
}

#MODULE INSTANCE WEBSERVER
module "devopslearn-module-webserver" {
  source = "./modules/webserver"
  instance_type = var.instance_type
  public_key_location = var.public_key_location
  avail_zone = var.avail_zone
  env_prefix = var.env_prefix
  subnet_id = module.devopslearn-vpc.public_subnets[0]
  default_security_group_id = aws_security_group.devopslearn-dmz-sg.id
}

resource "aws_security_group" "devopslearn-dmz-sg" {
  vpc_id = module.devopslearn-vpc.vpc_id
  name = "devopslearn-dmz-sg"

  ingress {
    description = "SSH Access"
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Web Access from Nginx"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    description = "All ports"
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
  }

  tags = {
    Name : "${var.env_prefix}-default-dmz-sg"
  }
}