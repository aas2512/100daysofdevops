# TERRAFORM

#### Overview

- Objective: Create an EC2 and run a docker container inside;

Provision AWS Infrastructure:
- Custom VPC;
- Custom Subnet;
- Create Route table & Internet Gateway;
- Create Security Group (Firewall)
- Provision EC2 Instance;
- User data to install Docker;
- Deploy nginx Docker container;

 <p align="center">
  <img src="overview-project.png">
 </p>