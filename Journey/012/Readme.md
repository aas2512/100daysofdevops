
# KUBERNETES

## DevOps Learning

- ✍️ Instalação e configuração do multipass(VM com Virtualbox) com o microk8s e tambem do kind(outro gerenciador de k8s) para gerenciamento do cluster local.

#### MULTIPASS & MICROK8S

- Criando maquina virtual<br/>
 `multipass launch -n k8s -c 2 -m 4gb -d 20gb`

- Instalando o microk8s <br/>
 `multipass exec k8s -- sudo snap install microk8s --classic --channel=1.18/stable`

- Adicionar o usuario ubuntu ao grupo do mikrok8s<br/>
 `multipass exec k8s -- sudo usermod -a -G microk8s ubuntu`

- Dar as permissões<br/>
 `multipass exec k8s -- sudo chown -f -R ubuntu ~/.kube`

- Reiniciar a maquina<br/>
 `multipass restart k8s`

- Testando instalação do k8s
```
 multipass exec k8s -- /snap/bin/microk8s.kubectl create deployment nginx --image=nginx
 multipass exec k8s -- /snap/bin/microk8s.kubectl get pods
```

- Pegar o kube config e colar local
```
 multipass exec k8s -- /snap/bin/microk8s.kubectl config view --raw
 vim ~/.kube/config e colar não esquecendo de trocar o ip e colocar o ip da maquina virtual
```

- Adicionar mais um nó ao cluster é só repetir o processo do primeiro nó
```
multipass launch -n k8s2 -c 2 -m 2gb -d 20gb
multipass exec k8s2 -- sudo snap install microk8s --classic --channel=1.18/stable
multipass exec k8s2 -- sudo usermod -a -G microk8s ubuntu
multipass exec k8s2 -- sudo chown -f -R ubuntu ~/.kube
multipass restart k8s
```

- Listar maquinas virtuais<br/>
`multipass list`

- Acessar o master(k8s) e executar o comando para adicionar um novo nó
```
multipass shell k8s
microk8s add-node
#Exibirá o seguinte resultado >> Join node with: microk8s join 10.16.199.198:25000/LwhVrnyMmZMJZnZjlkNaOByDLhfbzNJG
```

- Colar o comando exibido acima no novo nó
```
multipass shell k8s2
microk8s join 10.16.199.198:25000/LwhVrnyMmZMJZnZjlkNaOByDLhfbzNJG
```

#### KIND

- Instalar o kind o linux
```
curl -Lo ./kind https://kind.sigs.k8s.io/dl/v0.11.1/kind-linux-amd64
chmod +x ./kind
mv ./kind /some-dir-in-your-PATH/kind
```

- Criar um cluster<br/>
`kind create cluster --name <nome-cluster>`

- Criar um cluster a partir de um config<br/>
`kind create cluster --confi <file.yaml> --name <nome-cluster>`

