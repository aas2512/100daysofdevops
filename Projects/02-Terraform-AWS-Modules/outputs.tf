
  output "public_ip_instance" {
  value =  module.devopslearn-module-webserver.public_ip_instance
}

output "private_ip" {
  value = module.devopslearn-module-webserver.private_ip
}
